private final String inputFileName = "/tmp/abaporu.jpg"; 
private final int SCALE_X = 300;
private final int SCALE_Y = 0;
private final float RADIUS = 3;

PImage image;

void settings () {
  image = loadImage (inputFileName);
  image.resize (SCALE_X, SCALE_Y);
  size (2*image.width, 2*image.height);
}
ArrayList<Ball> balls;
void setup(){
  colorMode(HSB);
  //colorMode(RGB);
  balls = new ArrayList<Ball>();
  background (255);
  image.loadPixels();
  
  for (float y = RADIUS/2; y < image.height; y += RADIUS) {
    for (float x = RADIUS/2; x < image.width; x += RADIUS) {
      Ball b = new Ball(image.pixels[floor(y)*image.width+floor(x)], x, y, RADIUS);
      balls.add (b);
    }
  }
  image.updatePixels();
}

void draw(){
  //image(image,0,image.height);
  for (Ball b : balls) {
    //for (int i = 0; i < 40000; ++i) //update iterations
    b.update();
    b.show('t', 0, 0);
    b.show('L', image.width, 0);
    b.show('l', 0, image.height);
    b.show('c', image.width, image.height);
  }
  //saveFrame("results/img-#####.jpg");
}
