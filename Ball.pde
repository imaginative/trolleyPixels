public class Ball {
  private color c;
  private float radius;
  private float starting_radius;
  
  private PVector starting_pos;
  private PVector pos;
  private PVector vel;
  private float distance;
  
  public Ball (color c, float pos_x, float pos_y, float radius) {
    this.c = c;
    starting_pos = new PVector (pos_x, pos_y);
    pos = new PVector (pos_x, pos_y);
    vel = new PVector (random (-1, 1), random (-1, 1));
    this.radius = radius;
    this.starting_radius = radius;
    distance = 2*radius;
    
    //show();
  }
  public void missDirect() {
    vel.x = random(-1,1);
    vel.y = random(-1,1);
    
    if (random(1) < 0.2)
        radius += 2;
    else if (radius > 5)
      radius -= 1;
  }
  
  public void show (char mode, int dx, int dy) {
    //limit pixels to a circle
    /*float kappaX = abs(width/2 - pos.x);
    float kappaY = abs(height/2 - pos.y);
    float kappa = sqrt(kappaX*kappaX+kappaY*kappaY);
    kappa /= height/2;
    fill (hue(c), saturation(c), brightness(c)-255*kappa);*/
    fill (c);
    noStroke();
    
    switch (mode){
      case 'c': ellipse (pos.x+dx, pos.y+dy, radius, radius); break;
      case 'r': rect (pos.x+dx-radius/2.0, pos.y+dy-radius/2.0, radius, radius); break;
      case 't': triangle (pos.x+dx, pos.y+dy+radius, pos.x+dx+radius*sin(PI/3.0), pos.y+dy-radius*cos(PI/3.0), pos.x+dx-radius*sin(PI/3.0), pos.y+dy-radius*cos(PI/3.0));
                break;
      case 'l': stroke(red(c)+15,green(c)+15,blue(c)+15);
                line (pos.x+dx, pos.y+dy+radius, pos.x+dx+radius*sin(PI/3.0), pos.y+dy-radius*cos(PI/3.0));
                break;
      case 'L': stroke(255-brightness(c)-10, 255-saturation(c)-10, 255-brightness(c)-10);
                line (pos.x+dx, pos.y+dy+radius, pos.x+dx+radius*sin(PI/3.0), pos.y+dy-radius*cos(PI/3.0));
                break;
    }
  }
  public void update () {
    if (pos.x+vel.x > starting_pos.x+distance || pos.x+vel.x < starting_pos.x - distance) {
      missDirect();
    }
    if (pos.y+vel.y > starting_pos.y+distance || pos.y+vel.y < starting_pos.y - distance) {
      missDirect();
    }
    if (pos.x < 0 || pos.x > width) {
      vel.x *= -1;
    }
    if (pos.y < 0 || pos.y > height) {
      vel.y *= -1;
    }
    pos.add(vel);    
  }
}
